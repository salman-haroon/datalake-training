from redshift_s3 import RedshiftS3Tests
from parameter_store import SSMParameter
from json_files import ReadJSON
from athena_s3 import S3AthenaTests
from s3_partitioning import S3PartioningTest
from dynamo_s3 import DynamoS3Tests

data = ReadJSON().load_JSON()

if __name__ == "__main__":
    b = SSMParameter()

    s3_athena_comparison = S3AthenaTests()
    s3_athena_comparison.db_comparison(data['sourceSumQuery'],data['targetSumQuery'], 'sum')
    s3_athena_comparison.db_comparison(data['source_countQuery'], data['target_countQuery'], 'count')


    s3_partitioning = S3PartioningTest()
    s3_partitioning.partitioning_test(data['sourceDistinct'],data['sourceDistinctCount'])

    dynamo_s3 = DynamoS3Tests()
    dynamo_s3.dynamo_s3_comparison('id', '2', 'dynamoAssignment/Emp.csv')   #pass 1 for cars.csv and 2 for Emp.csv

    redshift_s3 = RedshiftS3Tests()
    redshift_s3.redshift_s3_comparison(data['selectQuery'], '')   #Pass data['selectQuery'] in first parameter and empty string in second parameter to compare the full dataframe