import boto3
from parameter_store import SSMParameter
from db_helper import DBHelper



sm = SSMParameter()
helper = DBHelper()
s3 = boto3.resource('s3')
bucket = s3.Bucket(sm.get_ssm_parameter('myBucketName'))
bucket_name = sm.get_ssm_parameter('myBucketName')


class S3PartioningTest:
    @staticmethod
    def get_target_partition_results():
        #This function get the list of the object stored in my S3 bucket
        car = []
        for o in bucket.objects.filter(Prefix=sm.get_ssm_parameter('myPartitionKey')):
            cars = o.key[10:16]                #partition/Honda is the path that comes in cars, so to ignore partition/ we take index from 10-16
            car.append(cars)

        str1 = " "
        l = str1.join(car).replace('   ', ' ').replace('  ', ' ')  #To ignore and replace the extra spaces in it
        carList = l.split(' ')  # Convert to list
        carList.pop(0)
        return carList

    @staticmethod
    def get_target_partition_count():
        #This function gets the count of the objects stored in me S3 bucket
        count_obj = 0
        for i in bucket.objects.filter(Prefix=sm.get_ssm_parameter('myPartitionKey')):
            count_obj = count_obj + 1
        return (count_obj - 1)


    """
        This function compares the s3 partioning results
    """
    @staticmethod
    def partitioning_test(partition_query, partition_count_query):
        source_conn = helper.source_conn()
        df1 = helper.execute_query(partition_query, source_conn)

        source_partition = df1['distinctResult'].values.tolist() #Convert to list
        target_partition = h.get_target_partition_results()

        print(source_partition)
        print(target_partition)

        if (source_partition == target_partition):
            print('Matched')
        else:
            print('Not Matched')

        df2 = helper.execute_query(partition_count_query, source_conn)

        source_count = df2['distinctResult'][0]
        target_count = h.get_target_partition_count()

        print(source_count)
        print(target_count)

        if (source_count == target_count):
            print("Partition Count Matched")
        else:
            print('Partition Count Not Matched')

h = S3PartioningTest()