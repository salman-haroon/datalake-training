import io
import numpy as np
import boto3
from parameter_store import SSMParameter
import pandas as pd
from db_helper import DBHelper
from json_files import ReadJSON

my_JSON = ReadJSON()
data = my_JSON.load_JSON()
s3_client = boto3.client('s3', region_name=data['regionName'])
sm = SSMParameter()
helper = DBHelper()
bucket_name = sm.get_ssm_parameter('myBucketName')


class RedshiftS3Tests():
    """
        This function compares the data stored in Postgre and reads the csv from S3
        And compares them
        :param query, it is for the query for postgre database
        :param dataframe_query, it is used to query the data in the dataframe i.e getting a specific data from dataframe
    """
    @staticmethod
    def redshift_s3_comparison(query, dataframe_query):
        con = helper.postgres_connection()

        df1 = helper.execute_query(query, con)
        print(df1)

        df2 = h.read_csv_from_s3('Source/cars.csv')

        if (dataframe_query == ''):
            df1 = df1.astype(str)
            df2 = df2.astype(str)
            df1['pk_id_matched?'] = np.where((df1['pk_id'] == df2['pk_id']), 'True', 'False')
            df1['car_name_matched?'] = np.where((df1['car_name'] == df2['car_name']), 'True', 'False')
            df1['car_model_matched?'] = np.where((df1['car_model'] == df2['car_model']), 'True', 'False')
            df1['car_manufacturer_matched?'] = np.where((df1['car_manufacturer'] == df2['car_manufacturer']), 'True', 'False')
            print(df1)

        else:
            df = df2.query(dataframe_query)
            print(df)
            df1 = df1.astype(str)
            df = df.astype(str)
            df1['pk_id_matched?'] = np.where((df1['pk_id'] == df2['pk_id']), 'True', 'False')
            df1['car_name_matched?'] = np.where((df1['car_name'] == df2['car_name']), 'True', 'False')
            df1['car_model_matched?'] = np.where((df1['car_model'] == df2['car_model']), 'True', 'False')
            df1['car_manufacturer_matched?'] = np.where((df1['car_manufacturer'] == df2['car_manufacturer']), 'True', 'False')
            print(df1.equals(df))



    @staticmethod
    def read_csv_from_s3(key):
        obj = s3_client.get_object(Bucket=bucket_name, Key=key)
        df = pd.read_csv(io.BytesIO(obj['Body'].read()))
        return df

h = RedshiftS3Tests()
















