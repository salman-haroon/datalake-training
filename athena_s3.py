from parameter_store import SSMParameter
import pandas as pd
from db_helper import DBHelper
import numpy as np

sm = SSMParameter()
helper = DBHelper()

class S3AthenaTests:
    """
        This function compares the local DB (mysql) with athena on AWS
        :param source_query, to query mysql
        :param target_query, to query athena
    """
    @staticmethod
    def db_comparison(source_query, target_query, column_name):
        source_conn = helper.source_conn()
        df1 = helper.execute_query(source_query, source_conn)
        print(df1)

        target_conn = helper.target_conn()
        df2 = helper.execute_query(target_query, target_conn)
        print(df2)

        df1 = df1.astype(int)
        df2 = df2.astype(int)

        df1['results_matched?'] = np.where((df1[column_name] == df2[column_name]), 'True', 'False')
        print(df1)

