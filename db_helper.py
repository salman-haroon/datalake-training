from json_files import ReadJSON
from parameter_store import SSMParameter
import pymysql
import pandas as pd
import pyathena
import boto3
import psycopg2
import io

my_JSON = ReadJSON()
data = my_JSON.load_JSON()

s3 = boto3.resource('s3')
s3_client = boto3.client('s3', region_name=data['regionName'])

dynamo = boto3.client('dynamodb', region_name=data['regionName'])

sm = SSMParameter()
bucket = s3.Bucket(sm.get_ssm_parameter('myBucketName'))
bucket_name = sm.get_ssm_parameter('myBucketName')



class DBHelper():
    """
            This function is to connect with mysyql on local host
    """
    @staticmethod
    def source_conn():
        try:
            source_connection = pymysql.connect(sm.get_ssm_parameter('myHostname'), sm.get_ssm_parameter('myUsername'), sm.get_ssm_parameter('myPassword'), sm.get_ssm_parameter('myDatabase'))
            return source_connection
        except ConnectionError as e:
            print(e)

    """
        This fucntion is to connect with pyathena
    """
    @staticmethod
    def target_conn():
        try:
            target_connection = pyathena.connect(
                s3_staging_dir= sm.get_ssm_parameter('myS3BucketPath'),
                region_name=data['regionName'])
            return target_connection
        except ConnectionError as e:
            print(e)




    """
        This function is to connect with Postgresql on local host
    """
    @staticmethod
    def postgres_connection():
        try:
            connection = psycopg2.connect(user = 'postgres',
                                      password = data['postgrePassword'],
                                      host = data['postgreHost'],
                                      port = data['postgrePort'],
                                      database = data['postgreDatabase'])
            return connection
        except ConnectionError as e:
            print(e)

    """
        This function executes the query and returns the pandas dataframe
        :param query the query given to execute
        :param connection on which host the query should run
    """
    @staticmethod
    def execute_query(query, connection):
        try:
            df = pd.read_sql_query(query, connection)
            return df
        except SyntaxError as s:
            print(s)

helper = DBHelper()