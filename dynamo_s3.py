import boto3
from json_files import ReadJSON
from parameter_store import SSMParameter
from db_helper import DBHelper

my_JSON = ReadJSON()
data = my_JSON.load_JSON()
dynamo = boto3.client('dynamodb', region_name=data['regionName'])
sm = SSMParameter()
helper = DBHelper()
bucket_name = sm.get_ssm_parameter('myBucketName')
s3_client = boto3.client('s3', region_name=data['regionName'])

my_JSON = ReadJSON()
data = my_JSON.load_JSON()

class DynamoS3Tests:
    """
        This function does the S3 file information comparison with dynamoDB
        :param key, it gets key on which the item is store that we want to retrieve from column of dynamoBD
        :param value, it is the value given to that key in dynamoDB column
        :param prefix, it is the file stored in S3 bucket of which info we are comaparing in dynamoDB
    """
    @staticmethod
    def dynamo_s3_comparison(key, value, prefix):
        item = h.get_dynamo_items(key, value)
        dynamo_time_stamp = item['timeStamp']['S']
        dynamo_file_name = item['fileName']['S']
        dynamo_file_path = item['filePath']['S']
        print(dynamo_time_stamp, dynamo_file_path, dynamo_file_name)

        s3_item = h.get_S3_file_info(prefix)
        s3_time_stamp = str(s3_item['Contents'][0]['LastModified'])
        s3_file_name = s3_item['Contents'][0]['Key'][17:]
        s3_file_path = 's3://' + s3_item['Name'] + '/' + s3_item['Prefix']
        print(s3_time_stamp, s3_file_path, s3_file_name)

        if (dynamo_time_stamp == s3_time_stamp and dynamo_file_name == s3_file_name and dynamo_file_path == s3_file_path):
            print('File Information from S3 Landing and DynamoDB Matched')
        else:
            print('File Information from S3 Landing and DynamoDB Not Matched')

    @staticmethod
    def get_dynamo_items(key, value):
        response = dynamo.get_item(TableName='myTable', Key={key: {'N': value}})
        print(response['Item'])
        return response['Item']

    @staticmethod
    def get_S3_file_info(prefix):
        get_folder_objects = s3_client.list_objects(
            Bucket=bucket_name, Prefix = prefix,
        )

        return get_folder_objects

h = DynamoS3Tests()