import json

class ReadJSON():
    """
            This function loads the JSON file
    """
    @staticmethod
    def load_JSON():
        with open('config.json') as config_file:
            data = json.load(config_file)

        return data