from json_files import ReadJSON
import boto3

my_JSON = ReadJSON()
data = my_JSON.load_JSON()

ssm = boto3.client('ssm', region_name=data['regionName'])
class SSMParameter():
    """
            This fucntion gets the parameters from AWS Systems Manager Parameter Store
            :param parameter_name is the name of the parameter that we want to retrieve from parameter store
    """
    @staticmethod
    def get_ssm_parameter(parameter_name):
        parameter_store = ssm.get_parameter(Name=parameter_name, WithDecryption=True)
        parameter = parameter_store['Parameter']['Value']
        return parameter


